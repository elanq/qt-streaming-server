#ifndef STREAMSERVER_H
#define STREAMSERVER_H

#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <stdlib.h>
#include <assert.h>
#include <QObject>
#include "RTSPServer.hh"

class StreamServer:public RTSPServer
{
public:        
    static StreamServer *createNew(UsageEnvironment &env,
                                   Port rtspPort,
                                   UserAuthenticationDatabase *authDatabase,
                                   unsigned reclamationTestSeconds = 65);
    static void StartServer();
protected:
    StreamServer(UsageEnvironment &env,
                 int socket,
                 Port rtspPort,
                 UserAuthenticationDatabase *authDatabase,
                 unsigned reclamationTestSeconds);
    virtual ~StreamServer();    
protected:
    virtual ServerMediaSession *lookupServerMediaSession(const char *streamName);

};

#endif // STREAMSERVER_H

#-------------------------------------------------
#
# Project created by QtCreator 2014-03-26T23:37:31
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QT-StreamingServer
TEMPLATE = app


SOURCES += main.cpp\
        streamserver.cpp\
        mainwindow.cpp \
    streamingthread.cpp

HEADERS  += mainwindow.h\
        streamserver.h \
    streamingthread.h

FORMS    += mainwindow.ui

INCLUDEPATH+= /usr/include/vlc
DEPENDPATH += /usr/include/vlc

INCLUDEPATH += $$PWD/liveMedia/include
DEPENDPATH += $$PWD/liveMedia/include

INCLUDEPATH += $$PWD/BasicUsageEnvironment/include
DEPENDPATH += $$PWD/BasicUsageEnvironment/include
INCLUDEPATH += $$PWD/UsageEnvironment/include
DEPENDPATH += $$PWD/UsageEnvironment/include



INCLUDEPATH += $$PWD/groupsock/include
DEPENDPATH += $$PWD/groupsock/include



LIBS += -L/usr/lib -lvlc
LIBS += /usr/local/lib/libliveMedia.a\
        /usr/local/lib/libBasicUsageEnvironment.a\
        /usr/local/lib/libUsageEnvironment.a\
        /usr/local/lib/libgroupsock.a

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    const char * const vlc_args[] = {
        "-I","dummy",
        "--ignore-config",
        "--extraintf=logger",
        "--verbose=2"
    };

    ui->setupUi(this);
    thread = new QThread();
    stream = new StreamingThread();

    stream->moveToThread(thread);
    connect(stream,SIGNAL(workRequested()),thread,SLOT(start()));
    connect(thread,SIGNAL(started()),stream,SLOT(doWork()));
    connect(stream,SIGNAL(finished()),thread,SLOT(quit()),Qt::DirectConnection);

    stream->requestWork();
    videoFrame = ui->vidFrame;
    poller = new QTimer(this);
    isPlaying = false;

    // instantiate new vlc instance
//    vlcInstance = libvlc_new(sizeof(vlc_args) / sizeof(vlc_args[0]), vlc_args);
//    vlcInstance = libvlc_new(0,NULL);
//    //create media player environment
//    mp = libvlc_media_player_new(vlcInstance);

    connect(poller,SIGNAL(timeout()),this,SLOT(updateInterface()));

    // start timer to trigger every 100 ms to update interface slot
    poller->start(100);

}

MainWindow::~MainWindow()
{
    stream->abort();
    thread->wait();

    delete thread;
    delete stream;
    //stop playing
    libvlc_media_player_stop(mp);

    //free the media player
    libvlc_media_player_release(mp);
    libvlc_release(vlcInstance);

    delete ui;

}

void MainWindow::on_btnStartServer_clicked()
{
        QString filename = QFileDialog::getOpenFileName(this,tr("Open File"),QString(),tr("Video Files"));
        if(!filename.isEmpty())
        {
            QFile file(filename);
            if(!file.open(QIODevice::ReadOnly))
            {
                QMessageBox::critical(this,tr("Error"),tr("File tidak bisa dibaca"));
                return;
            }
            playFile(filename);
        }
}
void MainWindow::playFile(QString file)
{

    mt = libvlc_media_new_path(vlcInstance,file.toUtf8());
    libvlc_media_player_set_media(mp,mt);
    //embed vlc media instance to window
    int winId=videoFrame->winId();
    libvlc_media_player_set_xwindow(mp,winId);    \
    //play file
    libvlc_media_player_play(mp);
    isPlaying=true;
}

void MainWindow::updateInterface()
{
    if(!isPlaying)
    {
        return;
    }

    libvlc_media_t *currentMedia = libvlc_media_player_get_media(mp);

    if(currentMedia==NULL)
    {
        return;
    }
}

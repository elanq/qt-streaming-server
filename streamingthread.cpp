#include "streamingthread.h"
#include "streamserver.h"
#include <liveMedia.hh>
#include <BasicUsageEnvironment.hh>

#include <QTimer>
#include <QEventLoop>
#include <QThread>
#include <QDebug>

StreamingThread::StreamingThread(QObject *parent) : QObject(parent)
{
    workingFlag = false;
    abortFlag = false;
}

void StreamingThread::requestWork()
{
    mutex.lock();
    workingFlag = true;
    abortFlag = false;
    mutex.unlock();

    emit workRequested();
}

void StreamingThread::abort()
{
    mutex.lock();
    if(workingFlag)
    {
        abortFlag = true;
    }
    mutex.unlock();
}

void StreamingThread::doWork()
{
    TaskScheduler *scheduler = BasicTaskScheduler::createNew();
    UsageEnvironment *env = BasicUsageEnvironment::createNew(*scheduler);

    UserAuthenticationDatabase *authDB = NULL;

#ifdef ACCESS_CONTROL
    authDB = new UserAuthenticationDatabase;
    //replace with real string
    authDB->addUserRecord("username1","password1");
#endif

    //create RTSP instance with default port number 554
    //alternative port number is 8554
    RTSPServer *rtspServer;
    portNumBits rtspServerPortNum = 554;
    rtspServer = StreamServer::createNew(*env,rtspServerPortNum, authDB);

    if(rtspServer==NULL)
    {
        rtspServerPortNum = 8554;
        rtspServer = StreamServer::createNew(*env,rtspServerPortNum, authDB);
    }
    if(rtspServer==NULL)
    {
        *env << "Failed to create RTSP server : "<<env->getResultMsg() <<"\n";
    }

    *env << "play stream from this server using this URL \n";
    *env << rtspServer->rtspURLPrefix();

    scheduler->doEventLoop();

    mutex.lock();
    workingFlag = false;
    mutex.unlock();

    emit finished();
}

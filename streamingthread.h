#ifndef STREAMINGTHREAD_H
#define STREAMINGTHREAD_H

#include <QObject>
#include <QMutex>

class StreamingThread : public QObject
{
    Q_OBJECT
public:
    explicit StreamingThread(QObject *parent = 0);
    void requestWork();
    void abort();
private:
    bool abortFlag;
    bool workingFlag;
    QMutex mutex;
signals:
    void workRequested();
    void valueChanged(const QString &value);
    void finished();
public slots:
    void doWork();
};

#endif // STREAMINGTHREAD_H

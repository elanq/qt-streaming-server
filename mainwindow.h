#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#define POSTION_RESOLUTION 10000
#include <QMainWindow>
#include "vlc/vlc.h"
#include <QTimer>
#include <QFrame>
#include <QThread>
#include "streamingthread.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    QTimer *poller;
    QFrame *videoFrame;
    bool isPlaying;
    libvlc_instance_t *vlcInstance;
    libvlc_media_player_t *mp;
    libvlc_media_t *mt;


public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private:
    QThread *thread;
    StreamingThread *stream;
private slots:
    void on_btnStartServer_clicked();    
public slots:
    void playFile(QString file);
    void updateInterface();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H

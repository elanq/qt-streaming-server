#include "streamserver.h"
#include <liveMedia.hh>
#include <BasicUsageEnvironment.hh>
#include <string.h>

StreamServer::StreamServer(UsageEnvironment &env,
                           int socket,
                           Port rtspPort,
                           UserAuthenticationDatabase *authDb,
                           unsigned reclamationTestSeconds)
    :RTSPServer(env,socket,rtspPort,authDb,reclamationTestSeconds)
{

}

StreamServer::~StreamServer()
{

}
static ServerMediaSession *createNewSession(UsageEnvironment &env, char const *fileName /**fid*/);

//create singleton instance of streamserver
StreamServer *StreamServer::createNew(UsageEnvironment &env, Port rtspPort, UserAuthenticationDatabase *authDatabase, unsigned reclamationTestSeconds)
{
    int socket = setUpOurSocket(env,rtspPort);
    if(socket==-1)
    {
        return NULL;
    }
    return new StreamServer(env,socket,rtspPort,authDatabase,reclamationTestSeconds);
}

ServerMediaSession *StreamServer::lookupServerMediaSession(char const *streamName)
{
    // First, check whether the specified "streamName" exists as a local file:
      FILE* fid = fopen(streamName, "rb");
      Boolean fileExists = fid != NULL;

      // Next, check whether we already have a "ServerMediaSession" for this file:
      ServerMediaSession* sms = RTSPServer::lookupServerMediaSession(streamName);
      Boolean smsExists = sms != NULL;

      // Handle the four possibilities for "fileExists" and "smsExists":
      if (!fileExists) {
        if (smsExists) {
          // "sms" was created for a file that no longer exists. Remove it:
          removeServerMediaSession(sms);
        }
        return NULL;
      } else {
        if (!smsExists) {
          // Create a new "ServerMediaSession" object for streaming from the named file.
          sms = createNewSession(envir(), streamName);
          addServerMediaSession(sms);
        }
        fclose(fid);
        return sms;
      }
}
//handling matroska files
struct MatroskaDemuxCreationStat
{
    MatroskaFileServerDemux *demux;
    char watchVariable;
};
static void onMatroskaDemuxCreation(MatroskaFileServerDemux *newDemux, void *clientData)
{
    MatroskaDemuxCreationStat *creationState = (MatroskaDemuxCreationStat*)clientData;
    creationState->demux = newDemux;
    creationState->watchVariable = 1;
}

//handling OOG files
struct OggDemuxCreationState
{
    OggFileServerDemux *demux;
    char watchVariable;
};

static void onOggDemuxCreation(OggFileServerDemux *newDemux, void *clientData)
{
    OggDemuxCreationState *creationState = (OggDemuxCreationState*)clientData;
    creationState->demux = newDemux;
    creationState->watchVariable = 1;
}

#define NEW_SMS(description) do{\
char const* descStr = description\
    ", data streamed";\
    sms = ServerMediaSession::createNew(env,fileName,fileName,descStr);\
} while(0)

static ServerMediaSession *createNewSession(UsageEnvironment &env, char const *fileName /**fid*/)
{
    char const *extension = strrchr(fileName,'.');
    if(extension==NULL)
    {
        return NULL;
    }
    ServerMediaSession *sms = NULL;
    Boolean const reuseSource = False;

    if(strcmp(extension,".aac")==0)
    {
        NEW_SMS("AAC Audio");
        sms->addSubsession(ADTSAudioFileServerMediaSubsession::createNew(env,fileName,reuseSource));
    }else if(strcmp(extension,".amr")==0)
    {
        NEW_SMS("AMR Audio");
        sms->addSubsession(AMRAudioFileServerMediaSubsession::createNew(env,fileName,reuseSource));
    }else if(strcmp(extension,".ac3")==0)
    {
        NEW_SMS("AC3 Audio");
        sms->addSubsession(AC3AudioFileServerMediaSubsession::createNew(env,fileName,reuseSource));
    }else if(strcmp(extension,".m4e")==0)
    {
        NEW_SMS("MPEG-4 Video");
        sms->addSubsession(MPEG4VideoFileServerMediaSubsession::createNew(env,fileName,reuseSource));
    }else if(strcmp(extension,".264")==0)
    {
        NEW_SMS("H.264 Video");
        OutPacketBuffer::maxSize=100000;
        sms->addSubsession(H264VideoFileServerMediaSubsession::createNew(env,fileName,reuseSource));
    }else if(strcmp(extension,".265")==0)
    {
        NEW_SMS("H-265 Video");
        OutPacketBuffer::maxSize=100000;
        sms->addSubsession(H265VideoFileServerMediaSubsession::createNew(env,fileName,reuseSource));
    }else if(strcmp(extension,".mp3")==0)
    {
        NEW_SMS("Mp3 Audio");
        Boolean useADUs = False;
        Interleaving *interleaving = NULL;

#ifdef STREAM_USING_ADUS=
        useADUs = True;
#ifdef INTERLEAVE_ADUS
        unsigned char interleaveCycle[] = {0,2,1,3};
        unsigned char const interleaveCycleSize = (sizeof InterleaveCycle) / (sizeof unsigned char);
        interleaving = new Interleaving(interleaveCycleSize,interleaveCycle);
#endif
#endif
        sms->addSubsession(MP3AudioFileServerMediaSubsession::createNew(env,fileName,reuseSource,useADUs,interleaving));
        //sms->addSubsession(ADTSAudioFileServerMediaSubsession::createNew(env,fileName,reuseSource));
    }else if(strcmp(extension,".mpg")==0)
    {
        NEW_SMS("MPEG-1 or 2 Program Stream");
        MPEG1or2FileServerDemux *demux = MPEG1or2FileServerDemux::createNew(env,fileName,reuseSource);
        sms->addSubsession(demux->newVideoServerMediaSubsession());
        sms->addSubsession(demux->newAudioServerMediaSubsession());
    }else if(strcmp(extension,".vob")==0)
    {
        NEW_SMS("VOB ");
        MPEG1or2FileServerDemux *demux = MPEG1or2FileServerDemux::createNew(env,fileName, reuseSource);
        sms->addSubsession(demux->newVideoServerMediaSubsession());
        sms->addSubsession(demux->newAC3AudioServerMediaSubsession());

    }else if(strcmp(extension,".ts")==0)
    {
        unsigned indexFileNameLen = strlen(fileName)+2;
        char *indexFileName = new char [indexFileNameLen];
        sprintf(indexFileName,"%sx",fileName);
        NEW_SMS("MPEG Transport Stream");
        sms->addSubsession(MPEG2TransportFileServerMediaSubsession::createNew(env,fileName,indexFileName,reuseSource));
        delete[] indexFileName;
    }else if(strcmp(extension,".wav")==0)
    {
        NEW_SMS("WAV Audio Stream");
        Boolean convertToULaw = False;
        sms->addSubsession(WAVAudioFileServerMediaSubsession::createNew(env,fileName,reuseSource,convertToULaw));
    }else if(strcmp(extension,".dv")==0)
    {
        OutPacketBuffer::maxSize=300000;
        NEW_SMS("DV Video");
        sms->addSubsession(DVVideoFileServerMediaSubsession::createNew(env,fileName,reuseSource));
    }else if(strcmp(extension,".mkv")==0)
    {
        NEW_SMS("Matroska video audio and subtitles");
        MatroskaDemuxCreationStat creationState;
        creationState.watchVariable = 0;

        MatroskaFileServerDemux::createNew(env,fileName,onMatroskaDemuxCreation,&creationState);
        env.taskScheduler().doEventLoop(&creationState.watchVariable);

        ServerMediaSubsession *smss;
        while((smss= creationState.demux->newServerMediaSubsession())!=NULL)
        {
            sms->addSubsession(smss);
        }
    }else if(strcmp(extension,".ogg")==0 )
    {
        NEW_SMS("Ogg vide and/or audio");

        OggDemuxCreationState creationState;
        creationState.watchVariable=0;
        OggFileServerDemux::createNew(env,fileName,onOggDemuxCreation,&creationState);
        env.taskScheduler().doEventLoop(&creationState.watchVariable);

        ServerMediaSubsession *smss;
        while((smss = creationState.demux->newServerMediaSubsession())!=NULL)
        {
            sms->addSubsession(smss);
        }
    }
    return sms;
}

void StreamServer::StartServer()
{

}
